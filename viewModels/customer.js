module.exports = function(customer, orders = []) {
    const {
        customerId,
        firstName,
        lastName,
        email,
        info,
        phone,
    } = customer;
    return {
        customerId,
        email,
        info,
        phone,
        fullName: firstName + ' ' + lastName,
        orders: orders.map(order => ({
            orderId: order.orderId,
            category: order.category,
            price: order.getCurrency('USD')
        }))
    }
}