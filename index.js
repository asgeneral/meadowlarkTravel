const express = require('express');
const app = express();
const handlebars = require('express-handlebars');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const credentials = require('./credentials');
const expressSession = require('express-session');

// **Routers** //

const appConstance = require('./constance.js');


app.set('port', appConstance.port);

// **Templates engine** //
app.engine('.hbs', handlebars({ defaultLayout: 'main',extname: '.hbs'}));
app.set('view engine', '.hbs');

// **Middleware** //

app
    .use(express.static(appConstance.publicDir))
    .use(bodyParser.urlencoded({ extended: true}))
    .use(cookieParser(credentials.cookieSecret))
    .use(expressSession({
        resave: false,
        saveUninitialized: false,
        secret: credentials.cookieSecret,
    }));
// **Db middleware** //

const db = require('./db/index.js');
db.connect(app);
db.init(false);

// **some any controllers** //

require('./controllers/customer.js').registerRouter(app);
require('./controllers/home.js').registerRouter(app);

// **Error exceptions** //


app.use((req, res) => {
    res.status(404);
    res.render('404');
});

// **Listen server** //

app.listen(app.get('port'), () => console.log('\'server had started\''));