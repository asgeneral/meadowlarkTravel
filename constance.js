const path = require('path');
const appConstance = {
    port: process.env.PORT || 3000,
    publicDir: path.resolve(__dirname, 'public')
}

module.exports = appConstance;