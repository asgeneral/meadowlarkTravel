const Customer = require('../db/models/customer.js');
const customerViewModel = require('../viewModels/customer.js');

module.exports = {
    registerRouter: function (app) {
        app.get('/customer/:id', this.profileCustomer);
        app.get('/add-customer', this.addCustomer);
        app.post('/api/add-customer', this.apiAddCustomer);
        app.delete('/api/delete-customer-by-id', this.apiDeleteCustomer);
        app.get('/home', this.fake)
    },

    fake: (req, res) => res.send('Hello'),

    apiDeleteCustomer: (req, res) => {
        console.log('delete');
        const { customerId } = req.body;
        try {
            Customer.deleteOne({ customerId }, (err) => {
                console.log(err);
                if (err) {
                    return res.json({ errors: err })
                } else {
                    return res.json({ success: true });
                }
            });
        } catch (e) {
            res.json({ errors: e })
        }
    },

    apiAddCustomer: (req, res) => {
        const { firstName, lastName, phone, email } = req.body;
        const requiredFields = ['firstName', 'phone', 'email'];
        const errors = requiredFields.filter(field => {
            return !req.body[field];
        });
        if (errors.length) {
            return res.json({
                errors: errors.map(field => field + ' is empty!')
            })
        }
        try {
            Customer.find({ } , 'customerId', (err,  customers) => {
                const customerLength = customers.length;
                const customerId = customerLength ? customers[customerLength - 1].customerId + 1 : 0;

                const newCustomer = new Customer({
                    firstName,
                    lastName,
                    phone,
                    customerId,
                    email,
                    info: 'defaultInfo'
                });
                newCustomer.save();
                console.log(newCustomer);
                res.send({
                    newCustomer: customerViewModel(newCustomer),
                })
            })
        } catch (e) {
            res.json({ errors: e })
        }
    },

    profileCustomer: (req, res, next) => {
         try {
             Customer.find({ id: req.params.id }, function(err, [customer]) {
                 if (err)
                     return next(err);
                 if (!customer)
                     return res.redirect(303, '/add-customer');
                 res.render('customer/profile', customerViewModel(customer))
             })
         } catch (e) {
             res.json({ errors: e })
         }
    },

    addCustomer: (req, res) => {
        try {
            Customer.find({},  (err, customers) => {
                const sendCustomers = customers.map(customer => customerViewModel(customer));
                res.render('customer/add-customer', { currentCustomers: sendCustomers })
            })
        } catch (e) {
            res.json({ errors: e })
        }
    }
};