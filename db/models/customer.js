const mongoose = require('mongoose');
const Order = require('./order.js');

const customerSchema = mongoose.Schema({
    customerId: Number,
    firstName: String,
    lastName: String,
    email: String,
    dob: Date,
    info: String,
    address: String,
    phone: String
})

customerSchema.methods.getOrders = function (cb) {
    return Order.find({ customerId: this.id }, cb);
};

const Customer = mongoose.model('Customer', customerSchema);

module.exports = Customer;