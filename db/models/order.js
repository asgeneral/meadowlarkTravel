const mongoose = require('mongoose');
const orderSchema = mongoose.Schema({
    category: String,
    customerId: Number,
    orderDate: Date,
    price: Number
});

orderSchema.methods.getCurrency = function(currency, cb) {
    switch (currency) {
        case 'USD':
            return Math.floor(this.price * 0.1);
        case 'CENT':
            return Math.floor(this.price * 1);
        case 'BTC':
            return Math.floor(this.price * 0.0002232);
        default:
            throw new Error('unknown money currency');
    }
}

const Order = mongoose.model('Order', orderSchema);
module.exports = Order;