const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    id: Number,
    email: String,
    login: String,
})

const User = mongoose.model('User', userSchema);

module.exports = User;