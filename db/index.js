const credentials = require('../credentials');
const mongoose = require('mongoose');
const Order = require('./models/order.js');
const Customer = require('./models/customer.js');

const initialData = require('./initialData.js');

const opts = {
    useNewUrlParser: true,
}

module.exports = class {
    static connect(app) {
        switch (app.get('env')) {
            case 'development': {
                mongoose.connect(credentials.mongo.development.connectionString, opts);
                break;
            }
            case 'production': {
                mongoose.connect(credentials.mongo.production.connectionString, opts);
                break;
            }
            default:
                throw new Error('Unknown error' + app.get('env'));
        }
    }
    static init(rebase) {

        //** Removing collections **//

        if (rebase) {
            Order.deleteMany({}, (err) => {
                console.log('clean Order collection');
            });
            Customer.deleteMany({}, (err) => {
                console.log('clean Customer collection');
            });

            try {
                Order.insertMany(initialData.Orders, (err) => {
                    if (!err)
                        console.log('orders was inserted success');
                });
                Customer.insertMany(initialData.Customers, (err) => {
                    if (!err)
                        console.log('customers was inserted success');
                });
            } catch (e) {
                console.log(e);
            }
        }

        //** Set initial data this error exceptions **//

    }
};