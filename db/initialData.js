module.exports = {
    Orders: [
        { category: 'Nulla sollicitudin', customerId: 32, orderDate: new Date().setMonth(3), price: 3423 },
        { category: 'Quisque in', customerId: 33, orderDate: new Date().setMonth(1), price: 11 },
        { category: 'Aenean sit', customerId: 33, orderDate: new Date().setMonth(7), price: 2475 },
        { category: 'Nunc lacinia', customerId: 33, orderDate: new Date().setMonth(10), price: 11540 }
    ],

    Customers: [
        { id: 32, firstName: 'Fusce', lastName: 'Vivamus', email: 'asgeneral@yandex.ru', address: 'RF, Tomsk, Kitkina av. 43', phone: '3-323-232-23-11',
        info: 'Vestibulum lacinia laoreet luctus. Integer vehicula, diam at vehicula sodales, mi eros posuere mauris, sed malesuada mi orci non libero. Nam elementum nulla urna, nec'},
        { id: 33, firstName: 'Cras', lastName: 'Nullam', email: 'jogging-man@gmail.com', address: 'RF, Samara, Lenina st. 102', phone: '8-954-322-10-23',
        info: 'Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam tempor ex in turpis suscipit, sed lacinia velit bibendum. Maecenas sed egestas'}
    ]
}